# chessRobotSimulation_QT

> based on [Qt5.14.2](https://download.qt.io/archive/qt/5.14/5.14.2/), best performance with 1920\*1080 and 60 Hz

2021/05/27 learn qml, design the canvas

2021/05/30 start to build the whole structure of the stimulaition platform

2021/06/08 version 1.0 done!

2021/06/13 human vs AI done!

2021/10/24 UI update and startup settings added.

[BiliBili Video Link](https://www.bilibili.com/video/BV1zK411f7zJ)

[YouTube Viedo Link](https://youtu.be/V6IXxbrqHmE)

![image](https://user-images.githubusercontent.com/67775090/188526271-1db3713b-270d-4e3a-ba34-4fef4b50e9da.png)

![Screenshot from 2021-08-03 17-48-22](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-06-16.png)

![Screenshot from 2021-08-03 17-48-22](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2020-51-07.png)

![Screenshot from 2021-08-03 17-48-22](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-05-04.png)

![Screenshot from 2021-08-04 01-04-45](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-04-17.png)

![Screenshot from 2021-08-04 01-04-46](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-03-18.png)

![Screenshot from 2021-08-04 01-04-48](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-01-51.png)

![Screenshot from 2021-08-04 01-04-49](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2010-01-01.png)

![Screenshot from 2021-08-04 01-04-49](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2012-06-36.png)

![Screenshot from 2021-08-04 01-04-50](https://github.com/LeBronLiHD/chessRobotSimulation_QT/blob/master/chess/Screenshot%20from%202021-10-25%2012-06-38.png)
